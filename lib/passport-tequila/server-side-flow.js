Protocol = require('./protocol.js');

const ServerSideFlow = module.exports = function ServerSideFlow(opts) {
    if (! opts) { opts = {}; }
    const protocol = this.protocol = new Protocol();
    protocol.service = opts.service || "Some node.js app";
    protocol.request = opts.request;
    protocol.require = opts.require;
    protocol.allows = opts.allows;
    if (! opts.redirectUrl) {
      throw new Error("redirectUrl is required");
    }
    this.redirectUrl = opts.redirectUrl;

    ["tequila_host", "tequila_port", "tequila_createrequest_path", "tequila_requestauth_path",
     "tequila_fetchattributes_path", "tequila_logout_path"].forEach(function (k) {
           if (opts[k]) protocol[k] = opts[k];
        });
}

ServerSideFlow.prototype.validateTequilaReturn = async function(clientUrl) {
    const url = new URL(clientUrl);
    const key = url.searchParams.get("key");
    const auth_check = url.searchParams.get("auth_check");
    return new Promise((resolve, reject) => {
        try {
            if (! key) {
                reject(new Error("validateTequilaReturn: no key found in URL parameter"))
            }
            this.protocol.fetchattributes(key, auth_check, function(error, tequila_data) {
                if (error) {
                    reject(error);
                } else {
                    resolve(tequila_data);
                }
            });
        } catch (e) {
            reject(e);
        }
    })
}

ServerSideFlow.prototype.prepareLogin = async function() {
    return new Promise((resolve, reject) => {
        try {
            this.protocol.createrequest(this.redirectUrl, (error, result) => {
                if (error) { reject(error); return; }
                resolve(this.protocol.requestauthRedirectUrl(result));
            })
        } catch(e) {
            reject(e);
        }
    })
}
